# Latex Vorlage README
Dieses Repository dient als Grundlage für neue Projekte, 
Änderungen am Styling und an der Grundstruktur sollen wenn möglich zurückgepusht werden. 
[Github Gist](https://gist.github.com/kogakure/149016) stellt eine passende .gitignore Datei zur Verfügung.
